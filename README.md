# README #

### Arabic Number to Words ###

A python file to convert numbers to Arabic declarative (not ordinal) text phrases. Can be imported and its functions used or called form the command line by supplying input and output files and some other linguistic arguments. It also deals with some symbols and metric abbreviations as shown in code.

### Usage ###

Just import and use the function number_phrase_to_word(number_in, inflection, gender, nunation, genitive_follows) or use command line as follows:

```
#!cmd

$ words-to-numbers.py <input> <output> <inflection> <gender> <nutation> <genitive_following>
```
* input: dir of input file containing number/s and symbols ($£%...).
* output: dir of output file to hold output text.
* inflection: "subjective", "Accusative", "genitive" or "MSA"
* gender: "feminine" or "masculine".
* nunation: 0 or 1 whether to add ending nunation.
* genitive_follows: 0 or 1 whether there is a genitive following the number. Example: "200 cats" cats is a genitive but "the number 2000" is not followed by a genitive.

Or you can use pip to install it:
```
$ pip install -e git+https://bitbucket.org/nhalabi/numbers-to-words/.git#egg=Package
$ python
>>> from arabic_numbers_to_words import arabic_numbers_to_words
>>> arabic_numbers_to_words.number_phrase_to_word(number, inflection, gender, nunation, genitive_follows)
```
### Dependencies ###

There are no dependencies.

### Who do I talk to? ###

nawar.halabi@gmail.com