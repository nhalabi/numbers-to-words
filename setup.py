from setuptools import setup
from setuptools import find_packages
 
setup(
    name='arabic_numbers_to_words',    # This is the name of your PyPI-package.
    version='0.1.0',                   # Update the version number for new releases
	author="Nawar Halabi",
	author_email="nawar.halabi@gmail.com",
	license='MIT',
	packages=find_packages()
)